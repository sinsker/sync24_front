export const auth = {
    SET_USER: 'kako-auth/SET_USER',
    LOGIN: 'kako-auth/LOGIN',
    LOGOUT: 'kako-auth/LOGOUT',
    INITIALIZED: 'kako-auth/INITIALIZED'
}

//작성 폼
export const writeModal = {
    SET_WRITE: 'writeModal/SET_WRITE',
    OPEN: 'writeModal/OPEN',
    CLOSE: 'writeModal/CLOSE',
}