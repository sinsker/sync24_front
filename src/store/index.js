import Vue from 'vue'
import Vuex from 'vuex'
import { kakaoAuth } from './module/kakao-auth'
import { writeModal } from './module/write-modal'
import { getters }   from './getters'

Vue.use(Vuex);

const debug = process.env.NODE_ENV !== 'production'

export const store =  new Vuex.Store({
    getters,
    modules: {
        kakaoAuth,
        writeModal
    },
    strict: debug,
})