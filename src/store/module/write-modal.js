import * as types from '../mutation-types'

export const writeModal =  {
    state: {
        open: false,
        write: {}
    },
    mutations: {
        [types.writeModal.SET_WRITE] (state, write){
            state.write = write
        },
        [types.writeModal.OPEN] (state, flag){
            state.open = flag
        },
    },
    actions: {
        toggleWriteModal: ({commit}, flag) => {
            commit(types.writeModal.OPEN, flag);
        },
    }
}
