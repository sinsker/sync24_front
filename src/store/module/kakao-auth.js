import { KakaoModule }  from '../../utils/kakao'
import { KakaoAppConfig } from '../../config/kakao'
import { STORAGE } from '../../utils/storage'

import * as types from '../mutation-types'

export const kakaoAuth =  {
    state: {
        initialized: false,
        isLogin: false,
        user: null
    },
    mutations: {
        [types.auth.LOGIN] (state, user){
            state.isLogin = true
            state.user = user
        },
        [types.auth.LOGOUT] (state){
            state.isLogin = false
            state.user = {}
        },
        [types.auth.INITIALIZED] (state){
            state.initialized = true
        },
    },
    actions: {
        kakoInit: ({commit, state}) =>{
            Kakao.init(KakaoAppConfig.key)
            const user = STORAGE.get('user')
            if (STORAGE.get('user') !== null) {
                commit(types.auth.LOGIN, user)
            }
            commit(types.auth.INITIALIZED)
        },
        kakaoLogin:({commit}) => {
            return new Promise((resovle)=>{
                Kakao.Auth.login({
                    success: function (authObj) {
                        Kakao.API.request({
                            url: '/v1/user/me',
                            success: function (res) {
                                const user = {
                                    email: res.kaccount_email,
                                    image: res.properties.thumbnail_image,
                                    big_image: res.properties.profile_image,
                                    username: res.properties.nickname,
                                    id: res.id
                                }
                                STORAGE.set('user', user);
                                commit(types.auth.LOGIN, user)
                                resovle(res)
                            },
                            fail: function (error) {
                            }
                        })
                    },
                    fail: function (err) {
                    }
                })
            })
        },
        kakaoLogout: ({commit}) => {
            commit('LOGOUT')
            KakaoModule.logout()
            STORAGE.set('user', '')
        }
    }
}
