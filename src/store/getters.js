export const getters = {
    getIsLogin: state => state.kakaoAuth.isLogin,
    getLoginUser: state => state.kakaoAuth.user,
    getKakaoInitialized: state => state.kakaoAuth.initialized,

    getIsWriteModalOpen: state => state.writeModal.open
}