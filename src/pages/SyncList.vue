<template>
    <div id="sync-list">
        <loading-background :show="loading"></loading-background>
        <v-snackbar
                :timeout="2000"
                :bottom="true"
                v-model="snackbar.show"
        >
            {{ snackbar.massage }}
            <v-btn flat color="pink" @click.native="snackbar.show = false">닫기</v-btn>
        </v-snackbar>
        <sync-post-model
                :show="postModelOpen"
                :post="selectedPost"
                v-on:hide="hidePostModel"
        >
        </sync-post-model>
        <div class="option">
            <v-card class="option-card" style="height: 50px"  flat  color="secondary" v-if="meta.length > 0">
                <v-layout row wrap  >
                    <v-flex xs6>
                        <v-select dark  :value="optionsCreated[0]" :items="optionsCreated" overflow label="년도" @change="selectYear" ></v-select>
                    </v-flex>
                    <v-flex xs6 >
                        <v-select dark   :items="optionMonth" overflow label="전체"  @change="selectMonth"></v-select>
                    </v-flex>
                </v-layout>
            </v-card>
        </div>
        <v-container fluid grid-list-md class="grey lighten-4 option-list" >
            <v-layout column>
                <sync-card
                    v-for="post in posts"
                    :key="post.index"
                    :content="post"
                    v-on:remove="postRemove"
                    @click.native="showPostModel(post)"
                >
                </sync-card>
                <infinite-loading  @infinite="infiniteHandler" ref="infiniteLoading" >
                    <span slot="no-more">
                    </span>
                    <span slot="no-results">
                    </span>
                </infinite-loading>
            </v-layout>
        </v-container>
    </div>
</template>

<script>
    import LoadingBackground from "../components/LoadingBackground";
    import { HTTP } from "../utils/http-common";
    import { dragscroll } from 'vue-dragscroll'
    import * as helper from '../utils/helper';
    import InfiniteLoading from 'vue-infinite-loading';
    import SyncCard from "../components/SyncCard.vue";
    import SyncPostModel from "../components/SyncPostModel.vue";
    export default {
        components: {SyncCard, SyncPostModel, LoadingBackground, InfiniteLoading},
        directives: { dragscroll },
        name: 'SyncList',
        data()  {
            return  {
                posts:[],
                optionMonth:[],
                loading:false,
                total:0,
                meta:{},
                infinite:false,
                postModelOpen:false,
                selectedPost:{},
                condition:{
                    page:1,
                    year:'',
                    month:''
                },
                snackbar:{
                    show:false,
                    massage:''
                },
            }
        },
        created() {
            this.$nextTick(() => {
                this.$refs.infiniteLoading.$emit('$InfiniteLoading:complete');
            });
            this.postMetaCall().then((meta)=>{
                if(this.optionsCreated.length > 0){
                    this.condition.year = this.optionsCreated[0]['val'];
                    this.condition.month = '';
                    this.changeFilter();
                }
            });
        },
        computed:{
          queryString:function(){
              let condition = helper.objectToQuerystring(this.condition);
              return condition;
          },
          optionsCreated:function(){
              const dateList = this.meta;
              let options = [];

              if(this.meta.length == 0) return [];

              for(let index in dateList){
                  let year = dateList[index]['create_date_ym'].toString().substring(0, 4);
                  let month = dateList[index]['create_date_ym'].toString().substring(4, 6);

                  if(options.length === 0){
                      let temp = {
                          text: year + ' 년',
                          val : year,
                          month: [{text:'전체',val:''}]
                      };
                      options.push(temp);
                  }

                  for(let i in options){
                      if(options[i]['val'] !== year){
                          let temp = {
                              text: year + '년',
                              val : year,
                              month: [{text:'전체',val:''},{
                                  text: month + ' 월',
                                  val : month,
                              }]
                          };
                          options.push(temp);
                          break;
                      }

                      for(let j in options[i]['month']){
                          if(options[i]['month'][j]['val'] !== month){
                              let temp = {
                                  text: month + ' 월',
                                  val : month,
                              };
                              options[i]['month'].push(temp);
                              break;
                          }
                      }
                  }
              }
              if(options.length > 0)this.optionMonth = options[0]['month'];
              return options;
          }
        },
        methods:{
            postListCall:function(callback){
                this.$emit('before');
                this.loading = true;
                return HTTP.get(`post/${this.queryString}`)
                    .then(response => {
                        this.loading = false;
                        if(callback !== undefined) callback(response.data);
                    })
                    .catch(e => {
                        this.$emit('error');
                        this.loading = false;
                    });
            },
            postMetaCall:function(){
              return HTTP.get(`post-meta/`)
                    .then(response => {
                        this.meta = response.data.meta;
                        return this.meta;
                    })
                    .catch(e => {
                        this.loading = false;
                    });
            },
            selectYear:function(option){
                this.optionMonth = option.month;
                this.condition.year = option.val;
                this.condition.month = '';
                this.changeFilter();
            },
            selectMonth:function(option){
                this.condition.month = option.val;
                this.changeFilter();
            },
            postRemove:function(data){
                if(confirm('정말로 삭제하시겠습니까?')){
                    return HTTP.delete(`post/${data.index}/`)
                        .then(response => {
                            for(let i in this.posts){
                                if(this.posts[i]['index'] === data.index){
                                     this.posts.splice(i, 1);
                                    break;
                                }
                            }
                            this.showSnackbar('삭제가 완료되었습니다');
                        })
                        .catch(e => {
                        });
                }
            },
            showPostModel:function(post){
                this.postModelOpen = true;
                this.selectedPost = post;
            },
            hidePostModel:function(){
                this.postModelOpen = false;
            },
            showSnackbar:function(massage){
                this.snackbar.show = true;
                this.snackbar.massage = massage;
            },
            infiniteHandler($state) {
                this.postListCall((datas)=>{
                    if (datas.posts) {
                        this.posts = this.posts.concat(datas.posts);
                        $state.loaded();

                        this.condition.page++;
                        if (this.posts.length === 0 || this.posts.length >= datas.total) {
                            $state.complete();
                        }
                    }else{
                        $state.complete();
                    }
                });
            },
            changeFilter:function(){
                this.posts = [];
                this.condition.page = 1;
                this.$nextTick(() => {
                    this.$refs.infiniteLoading.$emit('$InfiniteLoading:reset');
                });
            }
        }
    }
</script>

<style scoped>
    .option {
        position: fixed;
        width: 100%;
        z-index: 5;
        top: 56px;
    }

    .option-card{

    }
    .option-list{
        margin-top: 52px;
    }


</style>
