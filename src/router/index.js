import Vue from 'vue'
import Router from 'vue-router'
//import SyncList from '../pages/SyncList.vue'
//import SyncInfo from '../pages/SyncInfo.vue'
//import SyncMain from '../pages/SyncMain.vue'
const SyncMain = () => import('../pages/SyncMain');
const SyncInfo = () => import('../pages/SyncInfo');
const SyncList = () => import('../pages/SyncList');

Vue.use(Router);

export default new Router({
  routes: [
    {
        path: '/',
        name: 'main',
        component: SyncMain
    },
      {
          path: '/info',
          name: 'info',
          component: SyncInfo
      },
    {
        path: '/list',
        name: 'list',
        component: SyncList,
        meta: { scrollToTop: true }
    },
  ]
})
