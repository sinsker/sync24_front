import router from './index'
import { store } from '../store'

export default router.beforeEach((to, from, next) => {
    // ...
    console.log(to, from, store.getters)
    if(!store.getters.getKakaoInitialized){
        store.dispatch('kakoInit');
    }

    next()
})