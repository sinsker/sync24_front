const isProduction = process.env.NODE_ENV === 'production'

let api = ''
if (isProduction) {
  api = require('./production').API
} else {
  api = require('./development').API
}

export default api.url
