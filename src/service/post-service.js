import Service from 'vue-service'
import url from '../config/deploy'

export default PostService extends Service {

    construct(){
      super(url)
    }

    function getPostAll(){
        return this.get('/post/');
    }

    function getPost(id) {
        return this.get('/post/'+ id);
    }

    function writePost(params){
        return this.post('/post/', params)
    }

    function updatePost(id, params){
        return this.put('/post/'+id+'/', params)
    }

    function deletePost(id){
        return this.delete('/post/'+id+'/')
    }
}