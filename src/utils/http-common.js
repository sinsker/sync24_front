import axios from 'axios'
import url from '../config/deploy'

export const HTTP = axios.create({
  baseURL: url,
  timeout: 10000,
  withCredentials: true,
  headers: {
    'Accept': 'application/json',
    'Content-Type': 'application/json'
  }
})
