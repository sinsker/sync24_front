export function objectToQuerystring(obj) {
    return Object.keys(obj)
        .filter(key => obj[key] !== '' && obj[key] !== null)
        .map((key, index) => {
            var startWith = index === 0 ? '?' : '&';
            return startWith + key + '=' + obj[key]
        }).join('');
}