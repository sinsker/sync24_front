import { STORAGE } from './storage'
import { KakaoAppConfig } from '../config/kakao'

export const KakaoModule = {
  key: KakaoAppConfig.key,
  initialized: false,
  isLogin: false,

  init: function () {
    if (this.initialized === false) {
      Kakao.init(this.key)
      this.initialized = true
      if (this.user() !== null) {
        this.isLogin = true
      }
    }
  },
  login: function (s, f) {
    this.init()
    const _this = this
    Kakao.Auth.login({
      success: function (authObj) {
        Kakao.API.request({
          url: '/v1/user/me',
          success: function (res) {
            STORAGE.set('user', res)
            _this.isLogin = true
            if (s !== undefined) s(res)
          },
          fail: function (error) {
            if (s !== undefined) f(err)
          }
        })
      },
      fail: function (err) {
        if (s !== undefined) f(err)
      }
    })
  },
  user: function () {
    return STORAGE.get('user')
  },
  logout: ()=>{
      STORAGE.set('user', '')
  }

}
