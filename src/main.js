import Vue from 'vue'
import App from './App.vue'
import Vuetify from 'vuetify'
import VueDragscroll from 'vue-dragscroll'
import { store } from './store'

import router from './router/index'
import beforeHook from './router/beforeHook'


Vue.use(Vuetify)
Vue.use(VueDragscroll)

new Vue({
  el: '#app',
  store,
  router,
  render: h => h(App)
})
